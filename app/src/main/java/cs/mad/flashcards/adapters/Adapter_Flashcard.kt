package cs.mad.flashcards.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.entities.Flashcard

class Adapter_Flashcard(input:List<Flashcard>):
RecyclerView.Adapter<Adapter_Flashcard.ViewHolder>(){

    private val mydata = input.toMutableList()



    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val textView = view.findViewById<TextView>(R.id.my_term)
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.item_flashcard_set, viewGroup, false))

    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.textView.text = mydata[position].term
    }

    override fun getItemCount(): Int {
        return mydata.size
    }
    fun addCard(flashcard: Flashcard): MutableList<Flashcard> {
        var flashcardnum = mydata.size + 1
        mydata += Flashcard("Term $flashcardnum", "Definition $flashcardnum")
        return mydata
    }

}
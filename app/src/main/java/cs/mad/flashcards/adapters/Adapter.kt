package cs.mad.flashcards.adapters

import android.content.Intent
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.activities.FlashcardSetDetailActivity

import cs.mad.flashcards.entities.FlashcardSet

class Adapter(input: List<FlashcardSet>) : RecyclerView.Adapter<Adapter.ViewHolder>() {
    private val mydata = input.toMutableList()

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val textView = view.findViewById<TextView>(R.id.my_set)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(View.inflate(parent.context, R.layout.item_flashcard, null))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.textView.text = mydata[position].title
        holder.textView.setOnClickListener() {
            holder.textView.context.startActivity(Intent(holder.itemView.context, FlashcardSetDetailActivity::class.java))
        }
    }

    override fun getItemCount(): Int {
        return mydata.size
    }

    fun addinfo(flashcardSet: FlashcardSet): MutableList<FlashcardSet> {
        var newcount = mydata.size + 1
        mydata += FlashcardSet("Set $newcount")
        return mydata

    }
}
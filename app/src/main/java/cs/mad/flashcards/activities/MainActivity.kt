package cs.mad.flashcards.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager

import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.adapters.Adapter
import cs.mad.flashcards.entities.FlashcardSet

/*
===================================================================================================================

     Reference documentation for recyclers: https://developer.android.com/guide/topics/ui/layout/recyclerview

===================================================================================================================
 */

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val recycler=findViewById<RecyclerView>(R.id.recycler)
        val adapter= Adapter(FlashcardSet.getHardcodedFlashcardSets())
        recycler.adapter= adapter
        recycler.layoutManager = GridLayoutManager(applicationContext,2)

        var newcount =11
        val addButton = findViewById<Button>(R.id.button)
        addButton.setOnClickListener(){
            adapter.addinfo(FlashcardSet("Set $newcount"))
            adapter.notifyDataSetChanged()
            Toast.makeText(applicationContext,"Added $newcount",Toast.LENGTH_SHORT).show()
            newcount+=1
        }

    }
}
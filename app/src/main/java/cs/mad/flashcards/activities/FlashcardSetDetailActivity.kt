package cs.mad.flashcards.activities

import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.adapters.Adapter_Flashcard
import cs.mad.flashcards.entities.Flashcard

class FlashcardSetDetailActivity: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.flashcard)
        val recycler2 = findViewById<RecyclerView>(R.id.recycler_2)
        val adapter2 = Adapter_Flashcard(Flashcard.getHardcodedFlashcards())

        recycler2.layoutManager = LinearLayoutManager(applicationContext)
        recycler2.adapter = adapter2

        var fCnum = 11

        val addButton = findViewById<Button>(R.id.addButton)
        addButton.setOnClickListener(){
            adapter2.addCard(Flashcard("Term $fCnum", "Definition $fCnum"))
            adapter2.notifyDataSetChanged()
            Toast.makeText(applicationContext,"Added Flashcard $fCnum!", Toast.LENGTH_SHORT).show()
            fCnum += 1
        }

    }
}


